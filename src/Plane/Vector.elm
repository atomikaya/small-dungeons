module Plane.Vector exposing (..)

{-| 2d vector manipulation
-}

import Plane.Point exposing (Point)


type alias Vector =
    { start : Point, end : Point }


unbind : Vector -> Point
unbind { start, end } =
    Point (end.x - start.x) (end.y - start.y)


magnitude : Vector -> Point
magnitude vector =
    unbind vector |> Plane.Point.map abs
