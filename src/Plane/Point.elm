module Plane.Point exposing (..)

{-| point in a positive plane
-}


type alias Point =
    { x : Int, y : Int }


blank : Point
blank =
    Point 0 0


toTuple : Point -> ( Int, Int )
toTuple point =
    ( point.x, point.y )


map : (Int -> Int) -> Point -> Point
map f { x, y } =
    Point (f x) (f y)


add : Point -> Point -> Point
add a b =
    Point (a.x + b.x) (a.y + b.y)


polarity : Point -> ( Order, Order )
polarity point =
    ( compare point.x 0, compare point.y 0 )


tilt : Point -> Order
tilt point =
    compare (abs point.x) (abs point.y)
