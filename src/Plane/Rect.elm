module Plane.Rect exposing (..)

{-| basic plane (euclidian?)
-}

import Plane.Point exposing (Point)
import Plane.Vector exposing (Vector)


type alias Rect =
    { size : Size, origin : Point }


type alias Size =
    { width : Int, height : Int }


blank : Rect
blank =
    Rect (Size 0 0) Plane.Point.blank

contains : Point -> Rect -> Bool
contains { x, y } { size, origin } =
    x
        >= origin.x
        && x
        < (origin.x + size.width)
        && y
        >= origin.y
        && y
        < (origin.y + size.height)


intersects : Int -> Rect -> Rect -> Bool
intersects distance r1 r2 =
    let
        v1 =
            toVector r1

        v2 =
            toVector r2
    in
    v1.start.x
        < v2.end.x
        + distance
        && v1.end.x
        + distance
        > v2.start.x
        && v1.start.y
        < v2.end.y
        + distance
        && v1.end.y
        + distance
        > v2.start.y


toVector : Rect -> Vector
toVector { origin, size } =
    Vector origin
        { x = origin.x + size.width - 1
        , y = origin.y + size.height - 1
        }
