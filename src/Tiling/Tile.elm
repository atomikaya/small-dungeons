module Tiling.Tile exposing (..)

{-| graphical tile utility for top-down 2D views
-}

import Azimuth exposing (Azimuth(..), Direction(..))
import Html exposing (Html)
import Html.Attributes exposing (style)
import Svg exposing (svg, use)
import Svg.Attributes exposing (viewBox, xlinkHref)


type Tile
    = Simple Collision String
    | OneOf Collision String Int
    | Stub Collision String (List Azimuth -> List Azimuth)
    | Surface Collision String (List Azimuth)


type Collision
    = Blocking
    | Passable


blank : Tile
blank =
    Simple Blocking ""


getCollision : Tile -> Collision
getCollision tile =
    case tile of
        Simple collision _ ->
            collision

        OneOf collision _ _ ->
            collision

        Stub collision _ _ ->
            collision

        Surface collision _ _ ->
            collision


getSource : Tile -> String
getSource tile =
    case tile of
        Simple _ source ->
            source

        OneOf _ source _ ->
            source

        Stub _ source _ ->
            source

        Surface _ source _ ->
            source


isPassable : Tile -> Bool
isPassable tile =
    case getCollision tile of
        Blocking ->
            False

        Passable ->
            True


toSurface : List Azimuth -> Tile -> Tile
toSurface winds tile =
    case tile of
        Stub c s func ->
            Surface c s (func winds)

        _ ->
            tile


fullSource : Tile -> String
fullSource tile =
    case tile of
        Simple _ source ->
            source

        OneOf _ source index ->
            source ++ String.fromInt index

        Stub _ source _ ->
            source

        Surface _ source winds ->
            if Azimuth.serialize winds == "" then
                source

            else
                source ++ "-" ++ Azimuth.serialize winds



-- SURFACE FILTERS


noFloatingOrdinal : List Azimuth -> List Azimuth
noFloatingOrdinal winds =
    List.foldl
        (\wind acc ->
            case wind of
                Cardinal _ ->
                    wind :: acc

                Ordinal a b ->
                    if
                        List.member (Cardinal a) winds
                            && List.member (Cardinal b) winds
                    then
                        wind :: acc

                    else
                        acc

                _ ->
                    acc
        )
        []
        winds


cardinalOnly : List Azimuth -> List Azimuth
cardinalOnly winds =
    List.filter
        (\wind ->
            case wind of
                Cardinal _ ->
                    True

                _ ->
                    False
        )
        winds



-- VIEW


view : Tile -> Html msg
view tile =
    if getSource tile == "" then
        Html.span [] []

    else
        svg
            [ style "width" "100%"
            , style "position" "absolute"
            , style "left" "0"
            , viewBox "0 0 40 40"
            ]
            [ use
                [ xlinkHref (fullSource tile)
                ]
                []
            ]
