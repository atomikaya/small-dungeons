module Tiling.Terrain exposing (..)

{-| top-down 2D tile area
-}

import Array exposing (Array)
import Azimuth exposing (Azimuth(..), Direction(..), relativePositions)
import Plane.Point exposing (Point)
import Plane.Rect
import Set exposing (Set)
import Tiling.Tile exposing (Tile)


type alias Terrain =
    Array (Array Tile)


blank : Terrain
blank =
    Array.empty


fromList : List (List Tile) -> Terrain
fromList list =
    List.foldl
        (\row acc -> Array.push (Array.fromList row) acc)
        Array.empty
        list


add : Point -> Terrain -> Terrain -> Terrain
add { x, y } terrain area =
    indexedFoldl
        (\p t acc ->
            update (Point (x + p.x) (y + p.y)) t acc
        )
        area
        terrain


get : Point -> Terrain -> Tile
get { x, y } area =
    Array.get y area
        |> Maybe.withDefault Array.empty
        |> Array.get x
        |> Maybe.withDefault Tiling.Tile.blank


update : Point -> Tile -> Terrain -> Terrain
update { x, y } tile area =
    indexedMap
        (\p t ->
            if x == p.x && y == p.y then
                tile

            else
                t
        )
        area


fill : Plane.Rect.Size -> Tile -> Terrain
fill { width, height } tile =
    tile |> Array.repeat width |> Array.repeat height


indexedMap : (Point -> Tile -> Tile) -> Terrain -> Terrain
indexedMap func area =
    Array.indexedMap
        (\y row ->
            Array.indexedMap (\x tile -> func (Point x y) tile) row
        )
        area


foldl : (Tile -> a -> a) -> a -> Terrain -> a
foldl func init area =
    area
        |> Array.foldl
            (\row yacc ->
                row |> Array.foldl (\tile xacc -> func tile xacc) yacc
            )
            init


indexedFoldl : (Point -> Tile -> a -> a) -> a -> Terrain -> a
indexedFoldl func init area =
    area
        |> Array.indexedMap Tuple.pair
        |> Array.foldl
            (\( y, row ) yacc ->
                row
                    |> Array.indexedMap Tuple.pair
                    |> Array.foldl
                        (\( x, tile ) xacc -> func (Point x y) tile xacc)
                        yacc
            )
            init


getNeighbors : Point -> Tile -> Terrain -> List Azimuth
getNeighbors { x, y } tile area =
    case tile of
        Tiling.Tile.Stub _ source _ ->
            List.foldr
                (\( dir, p ) acc ->
                    let
                        s =
                            Tiling.Tile.getSource <|
                                get (Point (x + p.x) (y + p.y)) area
                    in
                    if s == source || s == "" then
                        dir :: acc

                    else
                        acc
                )
                []
                relativePositions

        _ ->
            []


setupSurfaces : Terrain -> Terrain
setupSurfaces area =
    area
        |> indexedMap
            (\p tile ->
                case tile of
                    Tiling.Tile.Stub _ _ _ ->
                        if
                            List.length (getNeighbors p tile area)
                                == List.length relativePositions
                        then
                            Tiling.Tile.blank

                        else
                            Tiling.Tile.toSurface (getNeighbors p tile area) tile

                    _ ->
                        tile
            )



-- algorithm by @lydell
-- https://discourse.elm-lang.org/t/algorithm-for-shape-detection-in-2d-list/9797/2


passablePositions : Terrain -> List (List Point)
passablePositions area =
    -- Go through each cell, top to bottom, left to right
    area
        |> indexedFoldl
            (\pos _ ( visited, results ) ->
                let
                    ( newVisited, result ) =
                        flood area pos ( visited, [] )
                in
                ( newVisited
                , if List.isEmpty result then
                    results

                  else
                    List.reverse result :: results
                )
            )
            ( Set.empty, [] )
        |> Tuple.second
        |> List.reverse


flood :
    Terrain
    -> Point
    -> ( Set ( Int, Int ), List Point )
    -> ( Set ( Int, Int ), List Point )
flood area ({ x, y } as coordinate) ( visited, result ) =
    -- Don’t visit the same coordinate twice
    if Set.member (Plane.Point.toTuple coordinate) visited then
        ( visited, result )

    else
        case
            Array.get y area
                |> Maybe.andThen (Array.get x)
        of
            Just (Tiling.Tile.Simple Tiling.Tile.Passable _) ->
                -- Try all neighbors
                [ Point (x - 1) y, Point (x + 1) y, Point x (y - 1), Point x (y + 1) ]
                    |> List.foldl
                        (flood area)
                        ( Set.insert (Plane.Point.toTuple coordinate) visited
                        , coordinate :: result
                        )

            _ ->
                ( visited, result )


isPassable : Point -> Terrain -> Bool
isPassable pos area =
    Tiling.Tile.isPassable (get pos area)


passableLength : Terrain -> Int
passableLength area =
    foldl
        (\tile acc ->
            if Tiling.Tile.isPassable tile then
                acc + 1

            else
                acc
        )
        0
        area
