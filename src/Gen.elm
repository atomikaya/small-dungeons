module Gen exposing (..)

import Data.Rooms
import Dungeon.Room exposing (Room(..))
import List.Extra
import Plane.Point exposing (Point)
import Plane.Rect exposing (Rect, Size)
import Plane.Vector exposing (Vector)
import Random
import Array


rooms : Size -> Random.Generator (List (Room msg))
rooms size =
    Random.pair
        (Random.int 1 2)
        (Random.int 1 5)
        |> Random.andThen
            (\(fn, vn) ->
                Random.map2
                    List.append
                    (variableRooms size vn [])
                    (fixedRooms size fn Data.Rooms.templeRooms [])
            )


variableRooms :
    Size
    -> Int
    -> List (Room msg)
    -> Random.Generator (List (Room msg))
variableRooms size i acc =
    case i of
        0 ->
            Random.constant acc

        _ ->
            variableRoom size 3 8
            |> Random.andThen
                (\r ->
                    if roomIntersects r acc then
                        Random.constant acc
                    else
                        variableRooms size (i - 1) (r :: acc)
                )


fixedRooms :
    Size
    -> Int
    -> List (Room msg)
    -> List (Room msg)
    -> Random.Generator (List (Room msg))
fixedRooms size i list acc =
    case i of
        0 ->
            Random.constant acc

        _ ->
            Random.int 0 (List.length list - 1)
                |> Random.andThen
                    (\j ->
                        Array.fromList list
                            |> Array.get j
                            |> Maybe.withDefault Dungeon.Room.blank
                            |> fixedRoom size
                            |> Random.andThen
                                (\r ->
                                    if roomIntersects r acc then
                                        Random.constant acc
                                    else
                                        fixedRooms
                                            size
                                            (i - 1)
                                            (remove j list)
                                            (r :: acc)
                                )
                    )

remove : Int -> List a -> List a
remove index list =
    let
        arr = Array.fromList list
    in
    Array.slice 0 index arr
        |> Array.append (Array.slice (index + 1) (Array.length arr - 1) arr)
        |> Array.toList


roomIntersects : Room msg -> List (Room msg) -> Bool
roomIntersects r acc =
    List.any
        (Plane.Rect.intersects 1 (Dungeon.Room.getRect r))
        (List.map Dungeon.Room.getRect acc)

    -- then
    --     Random.constant acc

    -- else
    --     f size (i - 1) (r :: acc)


variableRoom : Size -> Int -> Int -> Random.Generator (Room msg)
variableRoom { width, height } min max =
    Random.pair
        (Random.int min max)
        (Random.int min max)
        |> Random.andThen
            (\( rw, rh ) ->
                Random.map2
                    (\x y ->
                        Variable (Rect (Plane.Rect.Size rw rh) (Point x y))
                    )
                    (Random.int 1 (width - rw - 1))
                    (Random.int 1 (height - rh - 1))
            )


fixedRoom : Size -> Room msg -> Random.Generator (Room msg)
fixedRoom { width, height } r =
    case r of
        Variable _ ->
            Random.constant r

        Fixed { size, origin } terrain items ->
            Random.map2
                (\x y ->
                    Fixed
                        (Rect size (Point (origin.x + x) (origin.y + y)))
                        terrain
                        items
                )
                (Random.int 1 (width - size.width - 1))
                (Random.int 1 (height - size.height - 1))


corridors : List (List Point) -> Random.Generator (List Vector)
corridors sas =
    case sas of
        [] ->
            Random.constant []

        _ :: [] ->
            Random.constant []

        s1 :: s2 :: [] ->
            Random.list 1 (corridor s1 s2)

        s1 :: s2 :: rest ->
            corridor s1 s2
                |> Random.andThen
                    (\line ->
                        Random.map
                            (\acc -> line :: acc)
                            (corridors (s2 :: rest))
                    )


corridor : List Point -> List Point -> Random.Generator Vector
corridor s1 s2 =
    let
        lget i list =
            List.Extra.getAt i list |> Maybe.withDefault (Point 0 0)
    in
    Random.map2
        (\i1 i2 -> Vector (lget i1 s1) (lget i2 s2))
        (Random.int 0 (List.length s1 - 1))
        (Random.int 0 (List.length s2 - 1))


point : List Point -> Random.Generator Point
point points =
    case points of
        [] ->
            Random.constant Plane.Point.blank

        p :: ps ->
            Random.uniform p ps


stairs : List (List Point) -> Random.Generator { up : Point, down : Point }
stairs allowedPos =
    point (List.concat allowedPos)
        |> Random.andThen
            (\up ->
                List.filter
                    (\p -> p.x == up.x && p.y == up.y |> not)
                    (List.concat allowedPos)
                    |> point
                    |> Random.map (\down -> { up = up, down = down })
            )
