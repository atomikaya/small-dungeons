module Data.Tiles exposing (..)

import Azimuth exposing (Azimuth(..))
import Tiling.Tile exposing (..)



-- TERRAIN


floor1 : Tile
floor1 =
    Simple Passable "tiles.svg#floor1"


floor2 : Tile
floor2 =
    Simple Passable "tiles.svg#floor2"


stairsUp : Tile
stairsUp =
    Simple Passable "tiles.svg#stairs-up"


stairsDown : Tile
stairsDown =
    Simple Passable "tiles.svg#stairs-down"


gateOut : Tile
gateOut =
    Simple Passable "tiles.svg#gate"


wall : Tile
wall =
    Stub Blocking "tiles.svg#wall" noFloatingOrdinal



-- PEOPLE


knight : Tile
knight =
    Simple Blocking "tiles.svg#knight"



-- ITEMS


carpet : Tile
carpet =
    Stub Passable "tiles.svg#carpet" cardinalOnly


angelStatue : Tile
angelStatue =
    Simple Blocking "tiles.svg#statue-angel"

leftBench : Tile
leftBench =
    Simple Passable "tiles.svg#bench-left"

rightBench : Tile
rightBench =
    Simple Passable "tiles.svg#bench-right"

altarLeft : Tile
altarLeft =
    Simple Blocking "tiles.svg#altar-left"

altarRight : Tile
altarRight =
    Simple Blocking "tiles.svg#altar-right"
