module Data.Rooms exposing (..)

import Data.Tiles as T
import Dungeon.Level exposing (Item(..))
import Dungeon.Room exposing (Room(..))
import Plane.Point exposing (Point)
import Plane.Rect exposing (Rect, Size)
import Tiling.Terrain



-- TEMPLE

templeRooms : List (Room msg)
templeRooms =
    [ angelOratory, emptyOratory, fullNave, halfNave, angelPews, angelTransept
    , loneAltar ]

angelOratory : Room msg
angelOratory =
    Fixed
        (Rect (Size 5 5) (Point 0 0))
        ([ [ T.wall, T.floor2, T.floor2, T.floor2, T.wall ]
         , List.repeat 5 T.floor2
         , List.repeat 5 T.floor2
         , List.repeat 5 T.floor2
         , [ T.wall, T.floor2, T.floor2, T.floor2, T.wall ]
         ]
            |> Tiling.Terrain.fromList
        )
        [ ( Point 1 1, Background T.carpet )
        , ( Point 2 1, Background T.carpet )
        , ( Point 3 1, Background T.carpet )
        , ( Point 1 2, Background T.carpet )
        , ( Point 2 2, Background T.angelStatue )
        , ( Point 3 2, Background T.carpet )
        , ( Point 1 3, Background T.carpet )
        , ( Point 2 3, Background T.carpet )
        , ( Point 3 3, Background T.carpet )
        ]


emptyOratory : Room msg
emptyOratory =
    Fixed
        (Rect (Size 5 5) (Point 0 0))
        ([ [ T.wall, T.floor1, T.floor1, T.floor1, T.wall ]
         , List.repeat 5 T.floor1
         , [ T.floor1, T.floor1, T.wall, T.floor1, T.floor1 ]
         , List.repeat 5 T.floor1
         , [ T.wall, T.floor1, T.floor1, T.floor1, T.wall ]
         ]
            |> Tiling.Terrain.fromList
        )
        [ ( Point 1 1, Background T.carpet )
        , ( Point 2 1, Background T.carpet )
        , ( Point 3 1, Background T.carpet )
        , ( Point 1 2, Background T.carpet )
        , ( Point 3 2, Background T.carpet )
        , ( Point 1 3, Background T.carpet )
        , ( Point 2 3, Background T.carpet )
        , ( Point 3 3, Background T.carpet )
        ]

fullNave : Room msg
fullNave =
    Fixed
        (Rect (Size 5 7) (Point 0 0))
        ([ [ T.wall, T.floor1, T.floor1, T.floor1, T.wall ]
         , List.repeat 5 T.floor1
         , List.repeat 5 T.floor1
         , List.repeat 5 T.floor1
         , List.repeat 5 T.floor1
         , List.repeat 5 T.floor1
         , [ T.wall, T.floor1, T.floor1, T.floor1, T.wall ]
         ]
            |> Tiling.Terrain.fromList
        )
        [ ( Point 0 1, Background T.leftBench )
        , ( Point 1 1, Background T.rightBench )
        , ( Point 2 1, Background T.carpet )
        , ( Point 3 1, Background T.leftBench )
        , ( Point 4 1, Background T.rightBench )
        , ( Point 0 2, Background T.leftBench )
        , ( Point 1 2, Background T.rightBench )
        , ( Point 2 2, Background T.carpet )
        , ( Point 3 2, Background T.leftBench )
        , ( Point 4 2, Background T.rightBench )
        , ( Point 0 3, Background T.leftBench )
        , ( Point 1 3, Background T.rightBench )
        , ( Point 2 3, Background T.carpet )
        , ( Point 3 3, Background T.leftBench )
        , ( Point 4 3, Background T.rightBench )
        , ( Point 0 4, Background T.leftBench )
        , ( Point 1 4, Background T.rightBench )
        , ( Point 2 4, Background T.carpet )
        , ( Point 3 4, Background T.leftBench )
        , ( Point 4 4, Background T.rightBench )
        , ( Point 0 5, Background T.leftBench )
        , ( Point 1 5, Background T.rightBench )
        , ( Point 2 5, Background T.carpet )
        , ( Point 3 5, Background T.leftBench )
        , ( Point 4 5, Background T.rightBench )
        ]

halfNave : Room msg
halfNave =
    Fixed
        (Rect (Size 6 4) (Point 0 0))
        (List.repeat
            4
            [ T.floor1, T.floor1, T.floor2, T.floor2, T.floor1, T.floor1 ]
            |> Tiling.Terrain.fromList
        )
        [ ( Point 0 0, Background T.leftBench )
        , ( Point 1 0, Background T.rightBench )
        , ( Point 4 0, Background T.leftBench )
        , ( Point 5 0, Background T.rightBench )
        , ( Point 0 1, Background T.leftBench )
        , ( Point 1 1, Background T.rightBench )
        , ( Point 4 1, Background T.leftBench )
        , ( Point 5 1, Background T.rightBench )
        , ( Point 0 3, Background T.carpet )
        , ( Point 1 3, Background T.carpet )
        , ( Point 2 3, Background T.carpet )
        , ( Point 2 3, Background T.altarLeft )
        , ( Point 3 3, Background T.carpet )
        , ( Point 3 3, Background T.altarRight )
        , ( Point 4 3, Background T.carpet )
        , ( Point 5 3, Background T.carpet )
        ]

angelPews : Room msg
angelPews =
    Fixed
        (Rect (Size 4 3) (Point 0 0))
        ([ List.repeat 4 T.floor1
         , List.repeat 4 T.floor1
         , List.repeat 4 T.floor1
         ]
            |> Tiling.Terrain.fromList
        )
        [ ( Point 0 0, Background T.angelStatue )
        , ( Point 3 0, Background T.angelStatue )
        , ( Point 1 1, Background T.leftBench )
        , ( Point 2 1, Background T.rightBench )
        , ( Point 1 2, Background T.carpet )
        , ( Point 2 2, Background T.carpet )
        ]

angelTransept : Room msg
angelTransept =
    Fixed
        (Rect (Size 9 4) (Point 0 0))
        ([ List.repeat 9 T.floor1
         , List.repeat 9 T.floor2
         , List.repeat 9 T.floor2
         , List.repeat 9 T.floor1
         ]
            |> Tiling.Terrain.fromList
        )
        [ ( Point 1 0, Background T.angelStatue )
        , ( Point 3 0, Background T.angelStatue )
        , ( Point 5 0, Background T.angelStatue )
        , ( Point 7 0, Background T.angelStatue )
        , ( Point 1 3, Background T.angelStatue )
        , ( Point 3 3, Background T.angelStatue )
        , ( Point 5 3, Background T.angelStatue )
        , ( Point 7 3, Background T.angelStatue )
        ]


loneAltar : Room msg
loneAltar =
    Fixed
        (Rect (Size 4 3) (Point 0 0))
        ([ [ T.wall, T.floor1, T.floor1, T.wall ]
         , List.repeat 4 T.floor1
         , List.repeat 4 T.floor1
         ]
            |> Tiling.Terrain.fromList
        )
        [ ( Point 1 1, Background T.altarLeft )
        , ( Point 2 1, Background T.altarRight )
        , ( Point 1 2, Background T.carpet )
        , ( Point 2 2, Background T.carpet )
        ]
