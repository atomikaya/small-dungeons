module Main exposing (..)

import Array exposing (Array)
import Azimuth exposing (Azimuth(..), Direction(..))
import Browser
import Browser.Events
import Data.Tiles
import Dungeon.Level exposing (Level)
import Dungeon.Room exposing (Room(..))
import Gen
import Html exposing (Html, aside, div, h1, main_, p, text)
import Html.Attributes exposing (class, height, style, width)
import Json.Decode as Decode
import Plane.Point exposing (Point)
import Plane.Rect
import Plane.Vector exposing (Vector)
import Platform.Cmd as Cmd
import Random
import Raster.Line
import Raster.Rect
import Task
import Tiling.Terrain exposing (Terrain)
import Tuple exposing (second)



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type Msg
    = Move Direction
    | PrevLevel
    | NextLevel
    | Activate Point
    | NewLevel (List (Room Msg))
    | NewCorridors (List Vector)
    | StairPos { up : Point, down : Point }
    | AskAgain


type alias SubArea =
    List Point


type alias Model =
    { size : Plane.Rect.Size
    , current : Int
    , levels : Array (Level Msg)
    }



-- HELPERS


currentLevel : Model -> Level Msg
currentLevel model =
    Array.get model.current model.levels |> Maybe.withDefault Dungeon.Level.blank


updateLevel : Model -> Level Msg -> Model
updateLevel model level =
    { model | levels = Array.set model.current level model.levels }


prevLevel : Model -> Model
prevLevel model =
    if model.current <= 0 then
        model

    else
        { model | current = model.current - 1 }


nextLevel : Model -> Model
nextLevel model =
    if model.current >= 9 then
        model

    else
        { model | current = model.current + 1 }


initLevel : Model -> Model
initLevel model =
    if hasNextLevel model then
        model

    else
        { model | levels = Array.push Dungeon.Level.blank model.levels }


hasNextLevel : Model -> Bool
hasNextLevel model =
    not <| Array.get (model.current + 1) model.levels == Nothing


updateTerrain : Model -> Terrain -> Model
updateTerrain model terrain =
    let
        level =
            currentLevel model
    in
    updateLevel model { level | terrain = terrain }


enoughRoomSpace : Plane.Rect.Size -> Terrain -> Bool
enoughRoomSpace { width, height } area =
    Tiling.Terrain.passableLength area > (width * height // 3)



-- Recursively find nearby 1:s
-- INIT


init : () -> ( Model, Cmd Msg )
init _ =
    let
        s =
            Plane.Rect.Size 20 15
    in
    ( { size = s
      , current = 0
      , levels = Array.fromList [ Dungeon.Level.blank ]
      }
    , Random.generate NewLevel (Gen.rooms s)
    )


setupTerrain : Plane.Rect.Size -> List (Room msg) -> Terrain
setupTerrain size rooms =
    rooms
        |> List.foldl
            (\r ->
                case r of
                    Variable rect ->
                        Raster.Rect.draw Data.Tiles.floor1 rect

                    Fixed rect terrain _ ->
                        Tiling.Terrain.add rect.origin terrain
            )
            (Tiling.Terrain.fill size Data.Tiles.wall)


setupItems : List (Room Msg) -> Model -> Model
setupItems rooms model =
    List.foldl
        (\room acc ->
            case room of
                Variable _ ->
                    acc

                Fixed rect _ items ->
                    Dungeon.Level.addItems rect.origin items acc
        )
        (currentLevel model)
        rooms
        |> updateLevel model


setupCorridors : Terrain -> List Vector -> Terrain
setupCorridors area lines =
    let
        raster segment =
            Raster.Line.fromVector segment

        replace segment =
            Raster.Rect.replace Data.Tiles.floor1 Data.Tiles.wall segment
    in
    List.foldl
        (\segment acc ->
            acc
                |> replace (raster segment |> .start)
                |> replace (raster segment |> .end)
        )
        area
        lines


setupStairs : { up : Point, down : Point } -> Model -> Model
setupStairs stairs model =
    currentLevel model
        |> Dungeon.Level.addItems Plane.Point.blank
            (List.append
                (if model.current <= 0 then
                    [ ( stairs.up, Dungeon.Level.Background Data.Tiles.gateOut ) ]

                 else
                    [ ( stairs.up, Dungeon.Level.Action Data.Tiles.stairsUp PrevLevel ) ]
                )
                (if model.current >= 9 then
                    []

                 else
                    [ ( stairs.down, Dungeon.Level.Action Data.Tiles.stairsDown NextLevel ) ]
                )
            )
        |> Dungeon.Level.placeCharacter stairs.up
        |> updateLevel model



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- character
        Move West ->
            ( currentLevel model
                |> Dungeon.Level.moveCharacter (Point -1 0)
                |> updateLevel model
            , Cmd.none
            )

        Move East ->
            ( currentLevel model
                |> Dungeon.Level.moveCharacter (Point 1 0)
                |> updateLevel model
            , Cmd.none
            )

        Move North ->
            ( currentLevel model
                |> Dungeon.Level.moveCharacter (Point 0 -1)
                |> updateLevel model
            , Cmd.none
            )

        Move South ->
            ( currentLevel model
                |> Dungeon.Level.moveCharacter (Point 0 1)
                |> updateLevel model
            , Cmd.none
            )

        PrevLevel ->
            ( prevLevel model, Cmd.none )

        NextLevel ->
            if hasNextLevel model then
                ( nextLevel model, Cmd.none )

            else
                nextLevel model
                    |> initLevel
                    |> (\m ->
                            ( m
                            , Random.generate NewLevel (Gen.rooms model.size)
                            )
                       )

        Activate pos ->
            ( model, activate pos model )

        -- setup
        NewLevel rooms ->
            let
                terrain =
                    setupTerrain model.size rooms

                newModel =
                    terrain |> updateTerrain model |> setupItems rooms
            in
            if enoughRoomSpace model.size terrain then
                ( newModel
                , Random.generate
                    StairPos
                    (newModel
                        |> currentLevel
                        |> Dungeon.Level.emptyPositions
                        |> Gen.stairs
                    )
                )

            else
                ( model, Random.generate NewLevel (Gen.rooms model.size) )

        StairPos stairs ->
            ( setupStairs stairs model
            , Random.generate NewCorridors
                (currentLevel model
                    |> .terrain
                    |> Tiling.Terrain.passablePositions
                    |> Gen.corridors
                )
            )

        NewCorridors corridors ->
            let
                terrain =
                    setupCorridors
                        (currentLevel model |> .terrain)
                        corridors
                        |> Tiling.Terrain.setupSurfaces
                        |> updateTerrain model

                items m =
                    m
                        |> currentLevel
                        |> .items
                        |> Dungeon.Level.setupSurfaces
                        |> Dungeon.Level.updateItems (currentLevel m)
                        |> updateLevel m
            in
            ( terrain |> items
            , Cmd.none
            )

        _ ->
            ( model, Cmd.none )


activate : Point -> Model -> Cmd Msg
activate pos model =
    case getItem model pos of
        Just (Dungeon.Level.Action _ msg) ->
            Task.perform identity (Task.succeed msg)

        Just _ ->
            Cmd.none

        Nothing ->
            Cmd.none


getItem : Model -> Point -> Maybe (Dungeon.Level.Item Msg)
getItem model pos =
    List.filter
        (\( p, _ ) -> p.x == pos.x && p.y == pos.y)
        (currentLevel model).items
        |> List.head
        |> Maybe.map second



-- VIEW


view : Model -> Html Msg
view ({ size } as model) =
    let
        percentage s =
            95 // s |> String.fromInt

        cellSize =
            "round(min("
                ++ percentage size.width
                ++ "vw, "
                ++ percentage size.height
                ++ "vh), 1px)"

        repeat s =
            "repeat(" ++ String.fromInt s ++ ", " ++ cellSize ++ ")"
    in
    div
        [ class "level--pink"
        , style "display" "flex"
        , style "justify-content" "center"
        , style "align-items" "center"
        ]
        [ main_
            [ style "display" "grid"
            , style "grid" (repeat size.height ++ " / " ++ repeat size.width)
            , style "place-content" "center"
            , style "place-items" "stretch"
            , style "height" "100vh"
            ]
            (currentLevel model |> Dungeon.Level.view)
        , aside []
            [ h1 []
                [ "Ancient Temple 1." ++ (model.current + 1 |> String.fromInt) |> text ]
            , p [] [ text "Layout: Ancient Temple" ]
            , p [] [ "Level: " ++ (model.current + 1 |> String.fromInt) |> text ]
            , p [] [ text "Danger rating: 1" ]
            , p [] [ text "Biome: (todo)" ]
            ]
        ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Browser.Events.onKeyDown (decodeKey (currentLevel model).character)


decodeKey : Point -> Decode.Decoder Msg
decodeKey pos =
    Decode.map (toMsg pos) (Decode.field "key" Decode.string)


toMsg : Point -> String -> Msg
toMsg pos key =
    case key of
        "ArrowLeft" ->
            Move West

        "ArrowRight" ->
            Move East

        "ArrowUp" ->
            Move North

        "ArrowDown" ->
            Move South

        "Enter" ->
            Activate pos

        _ ->
            AskAgain
