module Raster.Line exposing (..)

{-| 2D lines answering constraints:

  - 2 points (start and end)
  - 2 rectangular segments, 1 horizontal and 1 vertical
  - the axis with the largest distance starts (major)

-}

import Azimuth exposing (Azimuth(..), Direction(..))
import Plane.Point exposing (Point)
import Plane.Rect exposing (Rect)
import Plane.Vector exposing (Vector)


type alias Line =
    { start : Rect, end : Rect }



-- toLine : Vector.Point -> Vector.Point -> Line
-- toLine p1 p2 =


fromVector : Vector -> Line
fromVector ({ start, end } as vector) =
    let
        -- line thickness, untested with anything other than 1
        thickness =
            1

        -- just a visual representation of turning a zero-based point into a 1-based size
        toSize n =
            n + 1

        -- size of the major segment
        major prop =
            Plane.Vector.magnitude vector |> prop |> toSize

        -- the minor segment deduces the thickness of the major one
        minor prop =
            major prop - thickness

        -- x or y auto-offset for starting point
        startOffset prop =
            prop start - (Plane.Vector.magnitude vector |> prop)

        -- same for the line end point
        endOffset prop =
            prop end - ((Plane.Vector.magnitude vector |> prop) - thickness)

        -- size constructor horizontal segment, priority refers to local functions major / minor
        hSize priority =
            Plane.Rect.Size (priority .x) thickness

        -- same for vertical one
        vSize priority =
            Plane.Rect.Size thickness (priority .y)
    in
    case Azimuth.fromVector vector of
        Cardinal North ->
            Line
                (Rect (vSize major) (Point (.x start) (startOffset .y)))
                (Rect (hSize minor) (Point (endOffset .x) (.y end)))

        HalfWind North North East ->
            Line
                (Rect (vSize major) (Point (.x start) (startOffset .y)))
                (Rect (hSize minor) (Point (endOffset .x) (.y end)))

        Ordinal North East ->
            Line
                (Rect (hSize major) start)
                (Rect (vSize minor) end)

        HalfWind East North East ->
            Line
                (Rect (hSize major) start)
                (Rect (vSize minor) end)

        Cardinal East ->
            Line
                (Rect (hSize major) start)
                (Rect (vSize minor) (Point (.x end) (endOffset .y)))

        HalfWind East South East ->
            Line
                (Rect (hSize major) start)
                (Rect (vSize minor) (Point (.x end) (endOffset .y)))

        Ordinal South East ->
            Line
                (Rect (hSize major) start)
                (Rect (vSize minor) (Point (.x end) (endOffset .y)))

        HalfWind South South East ->
            Line
                (Rect (vSize major) start)
                (Rect (hSize minor) (Point (endOffset .x) (.y end)))

        Cardinal South ->
            Line
                (Rect (vSize major) start)
                (Rect (hSize minor) (Point (endOffset .x) (.y end)))

        HalfWind South South West ->
            Line
                (Rect (vSize major) start)
                (Rect (hSize minor) end)

        Ordinal South West ->
            Line
                (Rect (hSize major) (Point (startOffset .x) (.y start)))
                (Rect (vSize minor) (Point (.x end) (endOffset .y)))

        HalfWind West South West ->
            Line
                (Rect (hSize major) (Point (startOffset .x) (.y start)))
                (Rect (vSize minor) (Point (.x end) (endOffset .y)))

        Cardinal West ->
            Line
                (Rect (hSize major) (Point (startOffset .x) (.y start)))
                (Rect (vSize minor) end)

        HalfWind West North West ->
            Line
                (Rect (hSize major) (Point (startOffset .x) (.y start)))
                (Rect (vSize minor) end)

        Ordinal North West ->
            Line
                (Rect (hSize major) (Point (startOffset .x) (.y start)))
                (Rect (vSize minor) end)

        HalfWind North North West ->
            Line
                (Rect (vSize major) (Point (.x start) (startOffset .y)))
                (Rect (hSize minor) (Point (endOffset .x) (.y end)))

        _ ->
            Line
                (Rect (hSize major) start)
                (Rect (vSize minor) (Point (.x end) (endOffset .y)))
