module Raster.Rect exposing (..)

import Plane.Rect exposing (Rect)
import Tiling.Terrain exposing (Terrain)
import Tiling.Tile exposing (Tile)


draw : Tile -> Rect -> Terrain -> Terrain
draw tile rect area =
    area
        |> Tiling.Terrain.indexedMap
            (\pos t ->
                if Plane.Rect.contains pos rect then
                    tile

                else
                    t
            )


replace : Tile -> Tile -> Rect -> Terrain -> Terrain
replace tile target rect area =
    area
        |> Tiling.Terrain.indexedMap
            (\pos t ->
                if Plane.Rect.contains pos rect then
                    if t == target then
                        tile

                    else
                        t

                else
                    t
            )
