module Azimuth exposing (..)

{-| 16-point compass for a plane
-}

import Plane.Point exposing (Point)
import Plane.Vector exposing (Vector)


type Azimuth
    = Origin
    | Cardinal Direction
    | Ordinal Direction Direction
    | HalfWind Direction Direction Direction


type Direction
    = North
    | East
    | South
    | West


toList : Azimuth -> List Direction
toList wind =
    case wind of
        Cardinal a ->
            [ a ]

        Ordinal a b ->
            [ a, b ]

        HalfWind a b c ->
            [ a, b, c ]

        _ ->
            []


fromVector : Vector -> Azimuth
fromVector { start, end } =
    orient ( start.x, start.y ) ( end.x, end.y )


orient : ( Int, Int ) -> ( Int, Int ) -> Azimuth
orient ( x1, y1 ) ( x2, y2 ) =
    let
        free =
            Plane.Vector.unbind <| Vector (Point x1 y1) (Point x2 y2)
    in
    case ( Plane.Point.polarity free, Plane.Point.tilt free ) of
        ( ( EQ, LT ), _ ) ->
            Cardinal North

        ( ( GT, LT ), LT ) ->
            HalfWind North North East

        ( ( GT, LT ), EQ ) ->
            Ordinal North East

        ( ( GT, LT ), GT ) ->
            HalfWind East North East

        ( ( GT, EQ ), _ ) ->
            Cardinal East

        ( ( GT, GT ), GT ) ->
            HalfWind East South East

        ( ( GT, GT ), EQ ) ->
            Ordinal South East

        ( ( GT, GT ), LT ) ->
            HalfWind South South East

        ( ( EQ, GT ), _ ) ->
            Cardinal South

        ( ( LT, GT ), LT ) ->
            HalfWind South South West

        ( ( LT, GT ), EQ ) ->
            Ordinal South West

        ( ( LT, GT ), GT ) ->
            HalfWind West South West

        ( ( LT, EQ ), _ ) ->
            Cardinal West

        ( ( LT, LT ), GT ) ->
            HalfWind West North West

        ( ( LT, LT ), EQ ) ->
            Ordinal North West

        ( ( LT, LT ), LT ) ->
            HalfWind North North West

        _ ->
            Origin


index : Azimuth -> Int
index wind =
    case wind of
        Ordinal North West ->
            1

        Cardinal North ->
            2

        Ordinal North East ->
            3

        Cardinal West ->
            4

        Cardinal East ->
            6

        Ordinal South West ->
            7

        Cardinal South ->
            8

        Ordinal South East ->
            9

        _ ->
            5


relativePositions : List ( Azimuth, Point )
relativePositions =
    [ ( Ordinal North West, Point -1 -1 )
    , ( Cardinal North, Point 0 -1 )
    , ( Ordinal North East, Point 1 -1 )
    , ( Cardinal West, Point -1 0 )
    , ( Cardinal East, Point 1 0 )
    , ( Ordinal South West, Point -1 1 )
    , ( Cardinal South, Point 0 1 )
    , ( Ordinal South East, Point 1 1 )
    ]


serialize : List Azimuth -> String
serialize winds =
    List.map index winds
        |> List.foldl (\n acc -> String.fromInt n ++ acc) ""
