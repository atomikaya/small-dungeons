module Dungeon.Room exposing (..)

import Dungeon.Level exposing (Item)
import Plane.Point exposing (Point)
import Plane.Rect exposing (Rect)
import Tiling.Terrain exposing (Terrain)


type Room msg
    = Variable Rect
    | Fixed Rect Terrain (List ( Point, Item msg ))


blank : Room msg
blank =
    Variable Plane.Rect.blank

getRect : Room msg -> Rect
getRect room =
    case room of
        Variable rect ->
            rect

        Fixed rect _ _ ->
            rect
