module Dungeon.Level exposing (..)

import Azimuth exposing (Azimuth, relativePositions)
import Data.Tiles
import Html exposing (Html, div)
import Html.Attributes exposing (style)
import Plane.Point exposing (Point)
import Tiling.Terrain exposing (Terrain)
import Tiling.Tile exposing (Tile(..))


type Item msg
    = Action Tile msg
    | Background Tile


type alias Level msg =
    { terrain : Terrain
    , items : List ( Point, Item msg )
    , character : Point
    }


blank : Level msg
blank =
    Level Tiling.Terrain.blank [] Plane.Point.blank


addItems : Point -> List ( Point, Item msg ) -> Level msg -> Level msg
addItems pos items level =
    { level
        | items = level.items ++ List.map (\entry -> moveItem pos entry) items
    }


updateItems : Level msg -> List ( Point, Item msg ) -> Level msg
updateItems level items =
    { level | items = items }


itemsAt : Point -> List ( Point, Item msg ) -> List (Item msg)
itemsAt pos items =
    List.foldl
        (\( p, item ) acc ->
            if p == pos then
                item :: acc

            else
                acc
        )
        []
        items


moveItem : Point -> ( Point, Item msg ) -> ( Point, Item msg )
moveItem p1 ( p0, item ) =
    ( Plane.Point.add p0 p1, item )


moveCharacter : Point -> Level msg -> Level msg
moveCharacter v level =
    let
        { x, y } =
            level.character
    in
    placeCharacter (Point (x + v.x) (y + v.y)) level


placeCharacter : Point -> Level msg -> Level msg
placeCharacter pos level =
    if
        Tiling.Terrain.isPassable pos level.terrain &&
        isPassable pos level.items
    then
        { level | character = pos }

    else
        level

 
isPassable : Point -> List (Point, Item msg) -> Bool
isPassable pos items =
    List.filter (\(p, _) -> p == pos) items
        |> List.map (\(_, item) -> item)
        |> List.all
            (\item ->
                case item of
                    Action tile _ ->
                        Tiling.Tile.isPassable tile
                    Background tile ->
                        Tiling.Tile.isPassable tile
            )


view : Level msg -> List (Html msg)
view { terrain, items, character } =
    let
        toGrid n =
            n + 1 |> String.fromInt
    in
    Tiling.Terrain.indexedFoldl
        (\p t acc ->
            div
                [ style "grid-column" (toGrid p.x)
                , style "grid-row" (toGrid p.y)
                , style "position" "relative"
                ]
                (Tiling.Tile.view t
                    :: (itemsAt p items |> viewItems)
                    ++ (if p == character then
                            [ Tiling.Tile.view Data.Tiles.knight ]

                        else
                            []
                       )
                )
                :: acc
        )
        []
        terrain


viewItems : List (Item msg) -> List (Html msg)
viewItems items =
    List.foldl
        (\item acc ->
            case item of
                Action tile _ ->
                    Tiling.Tile.view tile :: acc

                Background tile ->
                    Tiling.Tile.view tile :: acc
        )
        []
        items


hasItems : Point -> List ( Point, Item msg ) -> Bool
hasItems pos items =
    List.length (itemsAt pos items) > 0


emptyPositions : Level msg -> List (List Point)
emptyPositions { terrain, items } =
    Tiling.Terrain.passablePositions terrain
        |> List.map
            (\row ->
                List.filter (\p -> not <| hasItems p items) row
            )


getNeighbors : Point -> Item msg -> List ( Point, Item msg ) -> List Azimuth
getNeighbors pos item items =
    List.foldl
        (\( wind, vec ) acc ->
            let
                neighbor =
                    List.filter
                        (\( p, _ ) -> p == Plane.Point.add pos vec)
                        items
                        |> List.head
            in
            case ( neighbor, item ) of
                ( Just ( _, Background a ), Background b ) ->
                    if a == b then
                        wind :: acc

                    else
                        acc

                _ ->
                    acc
        )
        []
        relativePositions


setupSurfaces : List ( Point, Item msg ) -> List ( Point, Item msg )
setupSurfaces items =
    List.map
        (\( p, item ) ->
            case item of
                Background (Stub c s f) ->
                    ( p
                    , Background
                        (Tiling.Tile.toSurface
                            (getNeighbors p item items)
                            (Stub c s f)
                        )
                    )

                _ ->
                    ( p, item )
        )
        items
